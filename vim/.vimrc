" VIMRC for Guilhem Marion's sessions

" Vim-plug
call plug#begin()
"Plug 'dracula/vim', {'as': 'dracula' }  " Theme
Plug 'ervandew/supertab'              " <tab> completion
Plug 'ntpeters/vim-better-whitespace' " Extra whitespace culling
Plug 'junegunn/vim-easy-align'        " Easy aligment
Plug 'morhetz/gruvbox'                " Groove theme
Plug 'junegunn/goyo.vim'              " Distraction free editing (:Goyo [dim])
Plug 'tpope/vim-fugitive'             " Git wrapper
Plug 'airblade/vim-gitgutter'         " Git differ

" Installed for Rust support
Plug 'rust-lang/rust.vim'             " Rust support
Plug 'vim-syntastic/syntastic'        " Syntax checking
Plug 'majutsushi/tagbar'              " Tag bar
Plug 'w0rp/ale'                       " Linting (maybe redundant ?)

Plug 'wikitopian/hardmode'            " Hardo modo
call plug#end()
" :PlugInstall

" 256 colours terminal
set t_Co=256

" Set leader key
let mapleader = ","
let g:mapleader = ","
nmap <leader>w :w!<cr>
nnoremap <leader>q :qa<cr>

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Move backups, swap, and undo files in separate directories
set backupdir=.backup/,~/.backup/,/tmp//
set directory=.swp/,~/.swp/,/tmp//
set undodir=.undo/,~/.undo/,/tmp//

set history=50	" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set cursorline  " highlight current line
set showcmd		" display incomplete commands
set incsearch	" do incremental searching
set number      " Line numbers

" Don't use Ex mode, use Q for formatting
map Q gq

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
        au!

        " For all text files set 'textwidth' to 78 characters.
        autocmd FileType text setlocal textwidth=78

        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event handler
        " (happens when dropping a file on gvim).
        autocmd BufReadPost *
                    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                    \   exe "normal! g`\"" |
                    \ endif

    augroup END
endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langnoremap')
    " Prevent that the langmap option applies to characters that result from a
    " mapping.  If unset (default), this may break plugins (but it's backward
    " compatible).
    set langnoremap
endif

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
packadd matchit

map <F7> mzgg=G`z

set shiftwidth=4
" Use spaces instead of tabs for indentation
" tabstop     -> how many columns a tab counts for
" expandtab   -> whether to expand tabs as spaces
" shiftwidth  -> shift width of << and >>
" softtabstop -> colums when pressing Tab, might cause \t + spaces to appear
"                useless if expandtab is set
" always set autoindenting on
set tabstop=4 expandtab shiftwidth=4 softtabstop=0 smarttab
set smartindent

" Prevent mem leak
:autocmd BufWinLeave * call clearmatches()

"color dracula
"set termguicolors
set background=dark
"let g:gruvbox_termcolors=16
let g:gruvbox_contrast_light='hard'
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
" Deactivate background to use terminal's one
hi! Normal ctermbg=NONE guibg=NONE
hi! Normal ctermbg=NONE guibg=NONE

" See 80th column
if exists('+colorcolumn')
    set cc=80
    set textwidth=0
    set wrapmargin=0
    highlight ColorColumn ctermbg=darkgrey
endif

" Stop the record macro madness
nnoremap <leader>Q q
nnoremap q <Nop>

" Status line
set laststatus=2
" Format the status line
set statusline=\ %F%m%r%h\ %w\ %r%{getcwd()}%h\ \ \ L%l

" Plugin configuration

" --- supertab autocomplete ---
"let g:SuperTabMappingForward  = '<c-space>'
"let g:SuperTabMappingBackward = '<s-c-space>'
let g:SuperTabLongestEnhanced = 1

" Trim whitespace on save
" These don't work for me for now
" let g:strip_whitespace_on_save = 1
" let g:strip_max_file_size = 10000
let g:strip_only_modified_lines = 1
let g:show_spaces_that_precede_tabs = 1

" By default deactivate key repeats
let g:hardtime_default_on = 1
" Defaults for hardtime
"let g:list_of_normal_keys = ["h", "j", "k", "l", "-", "+", "<UP>", "<DOWN>", "<LEFT>", "<RIGHT>"]
"let g:list_of_visual_keys = ["h", "j", "k", "l", "-", "+", "<UP>", "<DOWN>", "<LEFT>", "<RIGHT>"]
"let g:list_of_insert_keys = ["<UP>", "<DOWN>", "<LEFT>", "<RIGHT>"]
"let g:list_of_disabled_keys = []

" Easy align configuration
" Return to enter mode, [column to align], space
vnoremap <silent> <Enter> :EasyAlign<cr>

" GitGutter max changes for printing signs
" use [c (prev. hunk) and ]c (next hunk)
"nmap ]h <Plug>GitGutterNextHunk
"nmap [h <Plug>GitGutterPrevHunk
let g:gitgutter_max_signs = 500  " default value

" Hard mode configuration
" autocmd VimEnter,BufNewFile,BufReadPost * silent! call HardMode()
nnoremap <leader>h <Esc>:call ToggleHardMode()<CR>

" Syntasti configuration
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Rust configuration

" Tagbar configuration
nmap <F8> :TagbarToggle<CR>
nmap <F9> :TagbarOpen j<CR>
let g:tagbar_compact = 1
autocmd BufEnter * nested :call tagbar#autoopen(0)

" Netrw configuration
let g:netrw_liststyle= 3
let g:netrw_banner = 0

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
