# Programs used on any computer

zsh  - advanced shell
vim  - text ed, IDE
git  - source code management
tmux - shell multiplexer
cmus - CLI music player

# Useful CLI programs

ag      - fast grep
ff      - fast find
ranger  - cli file manager (overheady though)
rofi    - dmenu alternative
stow    - symlink management
