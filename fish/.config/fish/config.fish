# GAM Fish environment configuration

set profiles /etc/profile.d/
set rust_env $HOME/.cargo/env
set nix_env $HOME/.nix-profile/etc/profile.d/nix.sh
# This allows for flatpaks and snaps to show up
set prof_env $profiles/apps-bin-path.sh $profiles/flatpak.sh
set to_source $rust_env $nix_env $prof_env

if type bass -q
    for sauce in $to_source
        if test -r $sauce
            bass source $sauce 2>/dev/null
        end
    end
end

# Add pip install location to PATH
set pip_bins = $HOME/.local/bin
if test -d $pip_bins
    set PATH $PATH $pip_bins
end

# Force tmux to assume 256 color mode most notably for keybindings
alias tmux="tmux -2"
# Force correct TERM for keybindings in micro
alias micro="TERM=xterm-256color command micro"
