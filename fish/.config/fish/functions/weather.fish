function weather -d "Pull local weather from wttr.in"
    set nargs (count $argv)
    if test $nargs -gt 0
        curl wttr.in/$argv[1]
    else
        curl wttr.in/Toulouse
    end
end
