# Defined in - @ line 2
function kiltes
	if pgrep TESV.exe 1>/dev/null 2>&1
        set tespid (pgrep TESV.exe)
        kill -9 $tespid
        echo "-> Killed $tespid"
    else
        echo "-> No TESV.exe running"
    end
end
