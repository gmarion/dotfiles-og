# Defined in - @ line 1
function randpwd --description 'Generates random 32-char passwd'
    openssl rand -base64 32
end
