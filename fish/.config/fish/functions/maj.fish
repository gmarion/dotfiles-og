# Defined in - @ line 1
function maj --description 'Updates whole system based on distro'
    set DISTRO (lsb_release -si)
    if test "$DISTRO" = "Ubuntu"
        sudo apt-get update && sudo apt-get upgrade -y
    end
end
